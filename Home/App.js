var app = angular.module('InsitePortal', ['ngRoute'])
console.log('app')

app.config(function ($routeProvider) {
    $routeProvider
        .when('/finance', {
            templateUrl: 'finance.html',
        })
})

app.controller('homeCtrl', function ($rootScope, $scope, $location) {
    $scope.isActive = function (viewLocation) {
        // console.log(viewLocation, $location.path())
        return viewLocation === $location.path()
    }
})

app.controller('financeCtrl', function ($rootScope, $scope, $location) {
    $scope.isActive = function (viewLocation) {
        // console.log(viewLocation, $location.path())
        return viewLocation === $location.path()
    }
})
